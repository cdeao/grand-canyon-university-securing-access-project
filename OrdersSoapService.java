//Cameron Deao
//CST-235
//1/21/2019
package business;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService()
public class OrdersSoapService {

	@WebMethod()
	public String sayHello(String name) {
	    System.out.println("Hello: " + name);
	    return "Hello " + name + "!";
	}
}
