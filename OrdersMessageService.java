//Cameron Deao
//CST-235
//2/17/2019
package business;

import java.sql.SQLException;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import beans.Order;

import javax.jms.ObjectMessage;

/**
 * Message-Driven Bean implementation class for: OrderMessageService
 */
@MessageDriven(
		activationConfig = { @ActivationConfigProperty(
				propertyName = "destination", propertyValue = "java:/jms/queue/Order"), @ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		}, 
		mappedName = "java:/jms/queue/Order")
public class OrderMessageService implements MessageListener {

	@EJB
	OrdersBusinessService dataService;
	
	OrdersBusinessService service;
 
    public OrderMessageService() {
     
    }
	
    public void onMessage(Message message) {
    	//The message variable becomes and instance of TextMessage.
    	if(message instanceof TextMessage) {
    		//A new variable is established and typecasted to TextMessage.
    		TextMessage displayMessage = (TextMessage)message;
    		try {
    			//The console displays the proper message with the use of the .getText functionality.
				System.out.println(displayMessage.getText());
			} catch (JMSException e) {
				e.printStackTrace();
			}
    	}
    	//The message variable becomes and instance of ObjectMessage.
    	if(message instanceof ObjectMessage) {
    		//A new variable is established and typecased to ObjectMessage.
    		ObjectMessage displayMessage = (ObjectMessage)message;
    		try {
    			Order testMessage = (Order)displayMessage.getObject();
				dataService.insertOrder(testMessage);
				System.out.println(displayMessage.getObject());
			} catch (SQLException | JMSException e) {
				e.printStackTrace();
			}
    	}
    }
}
