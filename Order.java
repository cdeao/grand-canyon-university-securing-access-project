//Cameron Deao
//CST-235
//1/27/2019
package beans;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Order")
public class Order implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Variables are initialized with dummy data to display.
	public String orderNumber;
	public String productName;
	public float productPrice;
	public int productQuantity;


	public Order(String orderNumber, String productName, int productPrice, int productQuantity) {
		this.orderNumber = orderNumber;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productQuantity = productQuantity;
	}
	
	public Order() {
		this.orderNumber = "";
		this.productName = "";
		this.productPrice = 0;
		this.productQuantity = 0;
	}
	
	//These setters were deleted due to an error that was being thrown.
	/*public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}
	
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}*/
	
	public String getOrderNumber() {
		return orderNumber;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public float getProductPrice() {
		return productPrice;
	}
	
	public int getProductQuantity() {
		return productQuantity;
	}
	
	//Method that is used for adding an order into the list.
	public Order newOder(Order order) {
		Orders.orders.add(order);
		return order;
	}
}
