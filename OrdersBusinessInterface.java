//Cameron Deao
//CST-235
//2/17/2019
package business;

import java.sql.SQLException;
import java.util.List;
import javax.ejb.*;
import beans.*;

@Local
public interface OrdersBusinessInterface {
	
	public List<Order> getOrder();
	public void setOrders(List<Order> orders);
	public void test();
	public void sendOrder(Order order);
	public void insertOrder(Order order) throws SQLException;
}
