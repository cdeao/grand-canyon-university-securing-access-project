//Cameron Deao
//CST-235
//2/24/2019

package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Inject;
import beans.Order;
import beans.Orders;
import beans.User;
import business.OrdersBusinessInterface;
import java.sql.*;

@ManagedBean
@ViewScoped

public class FormController {
	
	@Inject
	private OrdersBusinessInterface service;
	@EJB
	business.MyTimerService timer;
	Order order = new Order("TEST", "NEW TEST", 1, 2);
	
	//Commented out the onSubmit method since it's use is no longer necessary.
	/*public String onSubmit(User user) throws SQLException {
		//timer.setTimer(10000);
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, Object> params = fc.getExternalContext().getRequestMap();
		user = (User) params.get("user");
		//Calls the method within the Order.java to fill the list.
		order.newOder(order);
		service.test();
		//x.setOrders(null);
		//Methods are called to read in the orders followed by inserting a new order
		//and reading in the updated orders list in the database.
		getAllOrders();
		insertOrder();
		getAllOrders();
		return "TestResponse.xhtml";
	}*/
	
	//Redirects the user to the appropriate page based on logging off the application.
	public String onLogoff() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "TestResponse.xhtml?faces-redirect=true";
	}
	
	public String onFlash(User user) {
		FacesContext fc = FacesContext.getCurrentInstance();
		Flash params = fc.getExternalContext().getFlash();
		user = (User) params.put("user", this);
		return "TestResponse2.xhtml";
	}
	
	public OrdersBusinessInterface getService() {
		return service;
	}
	
	public String goBack() {
		return "TestForm.xhtml";
	}
	
	//New method that was created to establish a new order with variables and 
	//return the proper .xhtml response page.
	public String onSendOrder() {
		service.sendOrder(new Order("NEW TEST", "NEW TEST", 12,55));
		return "OrderResponse.xhtml";
	}
	
	private void getAllOrders() throws SQLException {
		//Variables are established for the connection, username, password, along with a sqlString that will
		//be used as a query tool to read in data from the database.
		List<Order> orders = new ArrayList<Order>();
		Connection conn = null;
		String sqlUserName = "postgres";
		String sqlPassword = "Movieman16";
		Statement sqlStatement = null;
		String sqlString = "SELECT * FROM testapp.ORDERS";
		try {
			//A connection is made to the database with the proper URL, username, and password.
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", sqlUserName, sqlPassword);
			sqlStatement = conn.createStatement();
			//The resultset variable is where the data is being read into and where the data will be 
			//initialized to the proper variables.
			ResultSet rs = sqlStatement.executeQuery(sqlString);
			while(rs.next()) {
				int ID = rs.getInt("ID");
				String productName = rs.getString("PRODUCT_NAME");
				float price = rs.getFloat("PRICE");
				System.out.println("ID:" + ID + " " + productName + " Price: $" + price);
				//orders.add(new Order(rs.getString("ORDER_NO"),rs.getString("PRODUCT_NAME"),rs.getInt("PRICE"),rs.getInt("QUANTITY")));
				//The connection is closed upon completing the while loop that reads in the data.
			}
			conn.close();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Connection Failure!!");
			e.printStackTrace();
		}
		finally {
			conn.close();
		}
	}
	
	private void insertOrder() throws SQLException {
		Connection conn = null;
		Statement insertStatement = null;
		String sqlUserName = "postgres";
		String sqlPassword = "Movieman16";
		//The insert string is used to specify what data in the database will be affected and what
		//variables will be placed within those data fields.
		String insertString = "INSERT INTO testapp.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('001122334455','This was inserted new', 25.00,100)";
		try {
			try {
				conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", sqlUserName, sqlPassword);
				//A statement is created and using that statement the insert string is executed to 
				//insert the data into the database.
				insertStatement = conn.createStatement();
				insertStatement.executeUpdate(insertString);
				//The statement and the database are closed upon completion.
				insertStatement.close();
				conn.close();
			} catch (SQLException e) {
				System.out.println("Failed insert!");
				e.printStackTrace();
			}
		}
		finally {
			insertStatement.close();
			conn.close();
		}
	}
	
	public String processingOrder() throws SQLException {
		
		getAllOrders();
		return "TestResponse.xhtml";
	}
}

