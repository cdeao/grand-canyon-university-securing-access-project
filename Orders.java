//Cameron Deao
//CST-235
//1/27/2019
package beans;


import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.*;

@ManagedBean(name = "Orders", eager = true)
@ViewScoped
public class Orders implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String orderNumber;
	public String orderName;
	public int productPrice;
	public int productQuantity;
	//Establishing the list.
	//Getters and setters are used for the list.
	public static ArrayList<Order> orders = new ArrayList<Order>();

	public void setOrdersList(Order order) {
		//Orders.orders = orders;
		orders.add(order);
	}
	
	public ArrayList<Order> getOrder() {
		return orders;
	}
}
