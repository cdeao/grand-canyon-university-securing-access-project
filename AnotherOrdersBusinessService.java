//Cameron Deao
//CST-235
//2/17/2019
package business;

import java.util.ArrayList;
import java.util.List;
import javax.jms.Queue;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Connection;
import javax.jms.ObjectMessage;
import java.sql.*;


import beans.Order;
//import beans.Orders;
/**
 * Session Bean implementation class OrdersBusinessService
 */
@Alternative
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
public class AnotherOrdersBusinessService implements OrdersBusinessInterface {

	
	
	
	List<Order> orders = new ArrayList<>();
	public String orderNumber;
	public String orderName;
	public int productPrice;
	public int productQuantity;
    /**
     * Default constructor. 
     */
    public AnotherOrdersBusinessService() {
    	orders.add(new Order("TEST NUMBER", "TEST NAME", 44, 55));
    }

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
       System.out.println("Hello from the OrdersBusinessService");
    }

    @Override
	public List<Order> getOrder() {
    	
		return orders;
	}

	@Override
	public void setOrders(List<Order> orders) {
		//this.orders = orders;
		
	}
	
	//Proper resources are used to establish the connection factory along with
	//the queue for the order message.
	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;
	
	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;
	
	public void sendOrder(Order order) {
		//Send a Message for an Order
		try 
		{
			Connection connection = connectionFactory.createConnection();
			Session  session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer messageProducer = session.createProducer((Destination) queue);
			TextMessage message1 = session.createTextMessage();
			message1.setText("This is test message");
			messageProducer.send(message1);
			ObjectMessage message2 = session.createObjectMessage();
			message2.setObject(order);
			messageProducer.send(message2);
			connection.close();
			//Sets the new order to the first index to be displayed in the response page.
			orders.set(0, order);
		} 
		catch (JMSException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void insertOrder(Order order) throws SQLException{
		java.sql.Connection conn = null;
		String sqlUserName = "postgres";
		String sqlPassword = "Movieman16";
		//The insert string is used to specify what data in the database will be affected and what
		//variables will be placed within those data fields.
		String insertString = "INSERT INTO testapp.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES(?,?,?,?)";
		try {
			try {
				conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", sqlUserName, sqlPassword);
				//A statement is created and using that statement the insert string is executed to 
				//insert the data into the database.
				PreparedStatement newData = conn.prepareStatement(insertString);
				newData.setString(1, order.getOrderNumber());
				newData.setString(2, order.getProductName());
				newData.setFloat(3, order.getProductPrice());
				newData.setInt(4, order.getProductQuantity());
				newData.execute();
				//The statement and the database are closed upon completion.
				newData.close();
				conn.close();
				//Sets the new order to the first index to be displayed in the response page.
				orders.set(0, order);
			} catch (SQLException e) {
				System.out.println("Failed insert!");
				e.printStackTrace();
			}
		}
		finally {
			conn.close();
		}
	}
}
